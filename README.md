# code-verifier-backend

## Getting started

## Code Verifier Backend
Este proyecto es el backend de una aplicación de verificación de códigos. Proporciona una API para la validación de códigos y manejo de usuarios.

## Dependencias
A continuación se enumeran las dependencias principales utilizadas en este proyecto:

Express: Framework de aplicación web de Node.js que proporciona una forma sencilla de manejar solicitudes HTTP.
Mongoose: Biblioteca de modelado de objetos MongoDB para Node.js, que permite interactuar con la base de datos MongoDB de manera sencilla.
bcrypt: Biblioteca para el hashing de contraseñas, utilizada para almacenar contraseñas de manera segura en la base de datos.
jsonwebtoken: Implementación de JSON Web Tokens (JWT) para generar y verificar tokens de autenticación.
dotenv: Módulo que carga variables de entorno desde un archivo .env en el entorno de desarrollo local.
Estas dependencias son esenciales para el funcionamiento del backend y se encargan de proporcionar las funcionalidades básicas de la aplicación, como el enrutamiento, la interacción con la base de datos y la autenticación de usuarios.

Scripts de NPM
A continuación se describen los scripts de NPM definidos en el archivo package.json y su funcionalidad:

start: Inicia el servidor de la aplicación en modo de producción.
dev: Inicia el servidor de la aplicación en modo de desarrollo utilizando nodemon, lo que permite reiniciar automáticamente el servidor al realizar cambios en los archivos fuente.
lint: Ejecuta el linter para realizar un análisis estático del código y verificar el cumplimiento de las convenciones de estilo definidas.
Estos scripts facilitan la ejecución y el desarrollo del proyecto al proporcionar comandos predefinidos para iniciar el servidor en diferentes modos y realizar análisis del código.

Variables de entorno
El archivo .env debe contener las siguientes variables de entorno:

PORT: El número de puerto en el que se ejecutará el servidor de la aplicación.
MONGODB_URI: La URL de conexión a la base de datos MongoDB.
JWT_SECRET: La clave secreta utilizada para firmar y verificar los tokens de autenticación JWT.
Estas variables de entorno son necesarias para configurar correctamente el entorno de desarrollo local y permitir que la aplicación se conecte a la base de datos y genere tokens de autenticación seguros.

Asegúrese de configurar estas variables de entorno antes de ejecutar la aplicación localmente.


