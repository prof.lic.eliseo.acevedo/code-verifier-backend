import express, { Request, Response } from "express";
import { KatasController } from "../controller/KatasController";
import { LogInfo } from "../utils/logger";
import { RequestWithUser } from "../domain/types/UsersResponse.type";

//upload file
import multer from 'multer';
const upload = multer();

// Body Parser to read BODY from requests
import bodyParser from "body-parser";

let jsonParser = bodyParser.json();

// JWT Verifier MiddleWare
import { verifyToken } from '../middlewares/verifyToken.middleware';
import { KataLevel, IKata, Rating } from "../domain/interfaces/IKata.interface";

// Router from express
let katasRouter = express.Router();


// http://localhost:8000/api/katas?id=6253dc47f30baed4c6de7f99
katasRouter.route('/')
    // GET:
    .get(verifyToken, async (req: Request, res: Response) => {
        // Obtain a Query Param (ID)
        let id: any = req?.query?.id;

        // Pagination - limit - difficulty - rating
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;
        let level: any = req?.query?.level;
        let stars: any = req?.query?.stars;

        LogInfo(`Query Param: ${id}`);
        // Controller Instance to excute method
        const controller: KatasController = new KatasController();
        // Obtain Reponse
        const response: any = await controller.getKatas(page, limit, level, stars, id)
        // Send to the client the response
        return res.status(200).send(response);
    })

    // DELETE:
    .delete(verifyToken, async (req: RequestWithUser, res: Response) => {
        // Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        // Obtain a userID Auth (ID)
        let userId: string = req.user.id;

        // Controller Instance to excute method
        const controller: KatasController = new KatasController();
        // Obtain Reponse
        const response: any = await controller.deleteKata(id, userId);
        // Send to the client the response
        return res.send(response);
    })

    // POST:
    .post(verifyToken, jsonParser, async (req: RequestWithUser, res: Response) => {

        // Obtain a userID Auth (ID)
        let creator: string = req.user.id;

        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || 'Default description';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let solution: string = req?.body?.solution || 'Default Solution';
        let participants: string[] = req?.body?.participants || [];
        let ratings: Rating[] = req?.body?.ratings || [];
        if (!ratings || ratings.length === 0) {
            ratings.push({
                userId: 'someUserId',
                score: 0
            });
        }


        if (name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0 && ratings) {
            // Controller Instance to excute method
            const controller: KatasController = new KatasController();

            let kata: IKata = {
                name: name,
                description: description,
                level: level,
                intents: intents,
                stars: stars,
                creator: creator,
                solution: solution,
                participants: participants,
                ratings: ratings
            }

            // Obtain Response
            const response: any = await controller.createKata(kata);

            // Send to the client the response
            return res.status(201).send(response);

        } else {
            return res.status(400).send({
                message: '[ERROR] Creating Kata. You need to send all attrs of Kata to update it'
            });
        }


    })

    //PUT:
    .put(jsonParser, verifyToken, async (req: RequestWithUser, res: Response) => {
        // Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        // Obtain a userID Auth (ID)
        let userId: string = req.user.id;

        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || '';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || '';
        let participants: string[] = req?.body?.participants || [];

        if (name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0) {
            // Controller Instance to excute method
            const controller: KatasController = new KatasController();

            let kata: IKata = {
                name: name,
                description: description,
                level: level,
                intents: intents,
                stars: stars,
                creator: creator,
                solution: solution,
                participants: participants,
                ratings: []
            }

            // Obtain Response
            const response: any = await controller.updateKata(id, userId, kata);

            // Send to the client the response
            return res.send(response);

        } else {
            return res.status(400).send({
                message: '[ERROR] Updating Kata. You need to send all attrs of Kata to update it'
            });
        }
    })

//PUT RATING: http://localhost:8000/api/katas/rating?id= 
katasRouter.put('/rating', jsonParser, verifyToken, async (req: RequestWithUser, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id;

    // Read from Auth
    let userId: any = req.user.id;
    // Read from body
    let score: number = req?.body?.score;

    if (id && userId && score) {
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();

        // Obtain Response
        const response: any = await controller.addRating(id, userId, score);

        // Send to the client the response
        return res.send(response);
    } else {
        return res.status(400).send({
            message: '[ERROR] Adding Rating. You need to send all attrs of Rating to add it'
        });
    }

})

//Post /attempt: http://localhost:8000/api/solve?id=
katasRouter.post('/solve', jsonParser, verifyToken, async (req: RequestWithUser, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id;
    // Read from Auth
    let userId: string = req.user.id;
    // Read from body
    let userSolution: string = req?.body?.userSolution;
    if (id && userId && userSolution) {
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();

        // Obtain Response
        const response: any = await controller.attemptKata(id, userId, userSolution);

        // Send to the client the response
        return res.send(response);
    } else {
        return res.status(400).send({
            message: '[ERROR] Attempting Kata. You need to send all required parameters'
        });
    }
})

//GET By userID
katasRouter.get('/mykatas', verifyToken, async (req: RequestWithUser, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id;

    // Read from Auth
    let userId: string = req.user.id;
    
    // Pagination - limit - difficulty - rating
    let page: any = req?.query?.page || 1;
    let limit: any = req?.query?.limit || 10;
    let level: any = req?.query?.level;
    let stars: any = req?.query?.stars;

    LogInfo(`Query Param: ${id}`);
    // Controller Instance to excute method
    const controller: KatasController = new KatasController();
    // Obtain Reponse
    const response: any = await controller.getKatas(page, limit, level, stars, id, userId)
    // Send to the client the response
    return res.status(200).send(response);
})

//Upload
katasRouter.post('/uploadFile', verifyToken, upload.single('file'), async (req: RequestWithUser, res: Response) => {
    try {
      const file = req.file;
      const token = req.user.token;
  
      /* if (!file || !token) {
        return res.status(400).send({
          message: 'Missing file or token',
        });
      } */
  
      // Process the result as needed
      // ...
  
      return res.send({
        message: 'File uploaded successfully',
        // result: result.data,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).send({
        message: 'Error uploading file',
      });
    }
  });
  

// Export Katas Router
export default katasRouter;


/**
 * 
 * Get Documents => 200 OK
 * Creation Documents => 201 OK
 * Deletion of Documents => 200 (Entity) / 204 (No return)
 * Update of Documents =>  200 (Entity) / 204 (No return)
 * 
 */

