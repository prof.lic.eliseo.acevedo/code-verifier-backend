import { Body, Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IKataController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { IKata, KataLevel, Rating } from "../domain/interfaces/IKata.interface";
//ORM - katas Collection
import { getAllKatas, getKataByID, deleteKataByID, createKata, updateKataByID, attemptKataByID} from "../domain/orm/Kata.orm";

@Route("/api/katas")
@Tags("KataController")
/**
 * Endpoint to retreive the Katas in the Collection "Katas" of DB 
 */
@Get("/")
export class KatasController implements IKataController {

    /**
     * Endpoint to retreive the katas in the Collection "Katas" of DB 
     * @param {string} id Id of Kata to retreive (optional)
     * @returns All katas o kata found by ID
     */
    @Get("/")
    public async getKatas(@Query() page: number, @Query() limit: number, @Query() level?: KataLevel, @Query() stars?: number, @Query() id?: string, userId?: string): Promise<any> {

        let response: any = '';
        
        if (id) {
            LogSuccess(`[/api/katas] Get Kata By ID: ${id} `);
            response = await getKataByID(id);
        } else {
            LogSuccess('[/api/katas] Get All Katas Request')
            const filter: any = {};
            if (userId) {
                filter.creator = userId;
            }
            if (level) {
                filter.level = level;
            }
            if (stars) {
                filter.stars = stars;
            }
            response = await getAllKatas(page, limit, filter);
        }

        return response;
    }

    @Post("/")
    public async createKata(kata: IKata): Promise<any> {

        let response: any = '';

        if (kata) {
            LogSuccess(`[/api/katas] Create New Kata: ${kata.name} `);
            await createKata(kata).then((r) => {
                LogSuccess(`[/api/katas] Created Kata: ${kata.name} `);
                response = {
                    message: `Kata created successfully: ${kata.name}`
                }
            });
        } else {
            LogWarning('[/api/katas] Register needs Kata Entity')
            response = {
                message: 'Kata not Registered: Please, provide a Kata Entity to create one'
            }
        }

        return response;
    }

    /**
     * Endpoint to delete the Katas in the Collection "Katas" of DB 
     * @param {string} id Id of Kata to delete (optional)
     * @returns message informing if deletion was correct
     */
    @Delete("/")
    public async deleteKata(@Query() id: string, userId: string): Promise<any> {

        let response: any = '';

        if (id) {
            LogSuccess(`[/api/katas] Delete kata By ID: ${id} `);
            let kata = await getKataByID(id);
            if (kata.creator === userId) {
                await deleteKataByID(id).then((r) => {
                    response = {
                        message: `kata with id ${id} deleted successfully`
                    }
                })
            } else {
                response = {
                    message: `You are not authorized to delete this kata`,
                    userMatch: false // Agregar respuesta para indicar que el usuario no coincide
                };
            }
        } else {
            LogWarning('[/api/katas] Delete kata Request WITHOUT ID')
            response = {
                message: 'Please, provide an ID to remove from database'
            }
        }

        return response;
    }

    @Put("/")
    public async updateKata(@Query() id: string, userId: string, kata: IKata): Promise<any> {

        let response: any = '';
        console.log('use y kata ------------------------->', userId, kata.creator)

        if (id) {
            LogSuccess(`[/api/katas] Update kata By ID: ${id} `);
            if (kata.creator === userId) {
                await updateKataByID(id, kata).then((r) => {
                    response = {
                        message: `kata with id ${id} updated successfully`
                    }
                })
            } else {
                response = {
                    message: `You are not authorized to update this kata`,
                    userMatch: false // Agregar respuesta para indicar que el usuario no coincide
                };
            }
        } else {
            LogWarning('[/api/katas] Update kata Request WITHOUT ID')
            response = {
                message: 'Please, provide an ID to update an existing kata'
            }
        }

        return response;
    }

    @Put("/rating")
    public async addRating(@Query() id: string, userId: string, score: number): Promise<any> {
        let response: any = '';

        if (id) {
            // Obtener la Kata por su ID
            LogSuccess(`[/api/katas/rating addRatind kata By ID: ${id} `);
            let kata = await getKataByID(id);

            // Check if the user has already added a score
            let existingRating = kata.ratings.find((rating: { userId: string; }) => rating.userId === userId);
            if (existingRating) {
                // The user has already added a score, another cannot be added
                throw new Error('El usuario ya ha agregado una puntuación a esta Kata');
            }

            // Crear un nuevo objeto Rating con la puntuación del usuario
            let userRating: Rating = {
                userId: userId,
                score: score
            };

            // Agregar la nueva puntuación a la propiedad ratings
            kata.ratings.push(userRating);

            // Calcular la media de las puntuaciones
            const totalPuntuacion = kata.ratings.reduce((suma: any, rating: { score: any; }) => suma + rating.score, 0);
            const mediaPuntuacion = totalPuntuacion / kata.ratings.length;

            // Calcular el número de estrellas según la media de puntuaciones
            const numEstrellas = Math.round((mediaPuntuacion / 5) * 5);

            // Actualizar el número de estrellas de la Kata
            kata.stars = numEstrellas;

            // Actualizar la Kata en la base de datos
            await this.updateKata(id, userId, kata);

            response = {
                message: `kata with id ${id} updated successfully`
            }
        } else {
            response = {
                message: 'Please, provide an ID to update an existing kata'
            }
        }

        return response;
    }

    @Post("/attempt")
    public async attemptKata(@Query() id: string, userId: string, userSolution: string): Promise<any> {
        let response: any = '';

        if (id && userId && userSolution) {
            LogSuccess(`[/api/katas/attempt] Attempting kata By ID: ${id} `);
            await attemptKataByID(id, userId, userSolution).then((r: any) => {
                response = r;
            })
        } else {
            LogWarning('[/api/katasattempt] Attempting kata Request WITHOUT ID or userId or solution')
            response = {
                message: 'Please, provide an ID, userId and solution to attempt a kata'
            }
        }


        return response;
    }

    //api/katas/upload
    @Post("/upload")
    uploadKataFile(): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
