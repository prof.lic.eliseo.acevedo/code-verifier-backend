import { BasicResponse } from "../types";
import { IUser } from "../../domain/interfaces/IUser.interface";
import { IKata, KataLevel } from "../../domain/interfaces/IKata.interface";

export interface IHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IUserController {
    // Read all users from database || get User By ID
    getUsers(page: number, limit: number, id?: string): Promise<any>
    // Get Katas of User
    getKatas(page: number, limit: number, id: string): Promise<any>
    // Delete User By ID
    deleteUser(id?: string): Promise<any>
    // Update user
    updateUser(id: string, user: any): Promise<any>
}

export interface IKataController {
    // Read all katas from database || get kata By ID
    getKatas( page: Number, limit: number, level?: KataLevel, stars?: number, id?: string, userId?: string): Promise<any>;
    // Create new kata
    createKata(kata: IKata): Promise<any>
   // Delete kata By ID
   deleteKata(id: string, userId: string): Promise<any>
   // Update kata
   updateKata(id: string, userId: string, kata: IKata): Promise<any>
   // Upload file kata
   uploadKataFile(): Promise<any>
}

export interface IAuthController { 
    // register users
    registerUser(user: IUser):  Promise<any>
    // login user
    loginUser(auth: any): Promise<any>
}
