// UsersResponse.type.ts
import { IUser } from "../interfaces/IUser.interface"
import { Request } from 'express';

export interface RequestWithUser extends Request {
  user?: any;
}

export type UserResponse = {
    users: IUser[],
    totalPages: number,
    currentPage: number
}
