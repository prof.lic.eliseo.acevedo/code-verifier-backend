import { kataEntity } from '../entities/Kata.entity';
import { IKata, KataLevel } from '../interfaces/IKata.interface';
import { LogSuccess, LogError } from "../../utils/logger";


// CRUD

/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */

export const getAllKatas = async (page: number, limit: number, filter: { creator?: string, level?: KataLevel, stars?: number }): Promise<any[] | undefined> => {
    try {
        let kataModel = kataEntity();

        let response: any = {};
        
        // Filters
        let filters: any = {};
        if (filter.creator) {
            filters.creator = filter.creator;
        }
        if (filter.level) {
            filters.level = filter.level;
        }
        if (filter.stars) {
            filters.stars = filter.stars;
        }
        console.log('------------------------------>',filter);

        // Search all Katas  (using pagination)
        await kataModel.find(filters)
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({ level: 1, stars: -1 }) // Sort by ascending difficulty level and descending score
            .exec().then((katas: IKata[]) => {
                response.katas = katas;
            });

        // Count total documents in collection "Katas"
        await kataModel.countDocuments(filters).then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;


    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Katas: ${error}`);
    }
}

// - Get kata By ID
export const getKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Search kata By ID
        return await kataModel.findById(id);

    } catch (error) {
        LogError(`[ORM ERROR]: Getting kata By ID: ${error}`);
    }
}

// - Delete Kata By ID
export const deleteKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Delete kata BY ID
        return await kataModel.deleteOne({ _id: id })

    } catch (error) {
        LogError(`[ORM ERROR]: Deleting kata By ID: ${error}`);
    }
}

// - Create New Kata
export const createKata = async (kata: any): Promise<any | undefined> => {

    try {

        let kataModel = kataEntity();
        return await kataModel.create(kata);

    } catch (error) {
        LogError(`[ORM ERROR]: Creating kata: ${error}`);
    }
}

// - Update kata By ID
export const updateKataByID = async (id: string, kata: any): Promise<any | undefined> => {

    try {

        let kataModel = kataEntity();

        // Update kata
        return await kataModel.findByIdAndUpdate(id, kata);

    } catch (error) {
        LogError(`[ORM ERROR]: Updating kata ${id}: ${error}`);
    }
}

// - Update attempt kata By ID
export const attemptKataByID = async (id: string, userId: string, userSolution: string): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();
        let kata = await kataModel.findById(id);
        if (kata) {
            // Check if the user's solution is correct
            if (userSolution === kata.solution) {
                let updatedKata = await kataModel.findOneAndUpdate(
                    { _id: id, 'participants.userId': userId },
                    { $inc: { 'participants.$.intents': 1 } },
                    { new: true }
                );
                if (!updatedKata) {
                    updatedKata = await kataModel.findByIdAndUpdate(
                        id,
                        { $push: { participants: { userId, intents: 1 } } },
                        { new: true }
                    );
                }
                return { message: 'Correct solution!', solution: kata.solution, attempts: updatedKata.participants.find((p: { userId: string }) => p.userId === userId).intents };
            } else {
                let updatedKata = await kataModel.findOneAndUpdate(
                    { _id: id, 'participants.userId': userId },
                    { $inc: { 'participants.$.intents': 1 } },
                    { new: true }
                );
                if (!updatedKata) {
                    updatedKata = await kataModel.findByIdAndUpdate(
                        id,
                        { $push: { participants: { userId, intents: 1 } } },
                        { new: true }
                    );
                }
                return { message: 'Incorrect solution. Try again!', attempts: updatedKata.participants.find((p: { userId: string }) => p.userId === userId).intents };
            }
        } else {
            throw new Error('Kata not found');
        }
    } catch (error) {
        LogError(`[ORM ERROR]: Attempting kata ${id}: ${error}`);
    }
}
