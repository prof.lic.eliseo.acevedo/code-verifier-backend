# Ejercicio 4b:

1) Muestra las primeras 5 ciudades que empiecen por A ordenadas de manera ascendente, las soluciones deben ser únicas:
|| db.Contacts.find({"location.city": /^A/}).sort({"location.city": 1}).limit(5)
-> compass:
seleccionar la colección Contacts. 
clic en la pestaña “Aggregations” y agregar $match con el filtro {"location.city": /^A/} para location.city empiece con la letra A.
Después, agregar $sort con el argumento {"location.city": 1} para ordenar l ascendente por el campo location.city. 
agregar $limit con el valor 5 para limitar.

2) Crea una colección a parte, que solo contenga a los contactos de Francia (France) y que tengan entre 18 y 50 años. Usa una agregación para ello.
|| db.Contacts.aggregate([
    { $match: { "location.country": "France", "dob.age": { $gte: 18, $lte: 50 } } },
    { $out: "ContactsFrance" }
    ])

-> compass:
seleccionar la colección Contacts. clic en “Aggregations” 
y agregar $match con el filtro { "location.country": "France", "dob.age": { $gte: 18, $lte: 50 } } 
y incluir solo aquellos documentos cuyo campo location.country sea igual a "France" y cuyo campo dob.age esté entre 18 y 50 años.

3) Añade un número favorito a cada contacto, luego crea un bucket agrupando por número favorito que separe en 5 segmentos.
|| a-> db.Contacts.updateMany({}, { $set: { favoriteNumber: 3 } })
|| b-> db.Contacts.aggregate([
    { $bucket: {
        groupBy: "$favoriteNumber",
        boundaries: [ 1, 10, 20, 30, 40, 50 ],
        default: "Other"
    } }
])
-> compass:
a) seleccionar Contacts. clic en la pestaña “Documents” clic en “Update Many”. 
En el cuadro que aparece, ingresar el filtro {} 
y la actualización { $set: { favoriteNumber: 3 } }. 
clic en el botón “Update” para ejecutar el comando.
b) seleccionar Contacts. Luego, clic en la pestaña “Aggregations” 
y agregar $bucket con { groupBy: "$favoriteNumber", boundaries: [ 1, 10, 20, 30, 40, 50 ], default: "Other" } 
para agrupar los documentos en la colección por su campo favoriteNumber y separarlos en 5 segmentos.


4) En la colección de Contatcs, haz una proyección la cual tiene que devolver solo el name y username del contacto.
|| db.Contacts.find({}, { "name": 1, "login.username": 1 })
filtro vacío {} para seleccionar todos. El segundo argumento del find() es una proyección que especifica 
qué campos se deben incluir o excluir en los documentos devueltos. En este caso, 
una proyección { "name": 1, "login.username": 1 } para incluir solo los campos name y login.username
-> compass:
seleccionar Contacts. clic en la pestaña “Documents” y luego en el botón “Options”. 
En el cuadro que aparece, ingresar el filtro {} para seleccionar todos
y la proyección { "name": 1, "login.username": 1 } para incluir solo los campos name y login.username en los documentos devueltos. 
clic en el botón “Apply” para aplicar el filtro y la proyección.

5) Haz una consulta en la colección de Contacts la cual devuelva un documento por cada nombre (name) y que sea único, ordenado por apellido (last), tienes que usar el operador $unwind.
|| db.Contacts.aggregate([
    { $unwind: "$name" },
    { $group: { _id: "$name.first", last: { $first: "$name.last" } } },
    { $sort: { last: 1 } }
])
-> compass:
seleccionar Contacts. clic en la pestaña “Aggregations” 
y agregar $unwind con  "$name" para desenrollar el campo name en cada documento.
{
    path: "$name",
    includeArrayIndex: "nameIndex",
    preserveNullAndEmptyArrays: true
}
Después, agregar a $group con el argumento { _id: "$name.first", last: { $first: "$name.last" } }
agrupar name.first y seleccionar el primer valor del campo name.last para cada grupo.
Finalmente, agregar $sort con el argumento { last: 1 } para ordenar los documentos resultantes de manera ascendente por el campo last.

6) Haz una proyección convirtiendo la fecha (date) a un formato DD-MM-AAAA, la nueva variable será fechaNacimiento
|| db.Contacts.aggregate([
    db.Contacts.aggregate([
    {
        $addFields: {
            fechaNacimiento: {
                $dateToString: {
                    format: "%d-%m-%Y",
                    date: { $toDate: "$dob.date" }
                }
            }
        }
    }
])

-> compass:

selecionar Contacts. clic en  Aggregations.
clic en Add Stage y seleccionar $addFields.
En el editor escribir "fechaNacimiento": y presionar Enter.
agrega el objeto fechaNacimiento en el editor:

fechaNacimiento: {
            $dateToString: {
                format: "%d-%m-%Y",
                date: { $toDate: "$dob.date" }
            }
        }

clic en Apply para aplicar la etapa y ver los resultados.



![ej-4](public/img/ej--4a.JPG)
![ej-4](public/img/ej--4b.JPG)
![ej-4](public/img/ej--4c.JPG)
![ej-4](public/img/ej--4d.JPG)
![ej-4](public/img/ej--4e.JPG)
![ej-4](public/img/ej--4f.JPG)
![ej-4](public/img/ej--4g.JPG)

------------------------------------------------------
------------------------------------------------------
# Ejercicio-4 (se adjunto capturas)

-Replicar proyecto completo Node con TS y Express visto en el vídeo
-Asegúrate de poder ver correctamente documentada tu API con Swagger

- Crea una base de datos en Mongo con Mongo Compass llamada “Pruebas”
- Importa el JSON a una colección llamada Contacts

- Hacer peticiones al servidor de Mongo desde Mongo Compass & Mongo Shell para hacer los siguientes ejercicios:
- Listar todos los contactos.--> db.Contacts.find()
- Busca el primer contacto que sea de Alemania (Germany): 
{"location.country": "Germany"} || db.Contacts.findOne({"location.country": "Germany"})
- Busca todos los contactos que tengan Blake como nombre (first): 
{"name.first": "Blake"} || db.Contacts.find({"name.first": "Blake"})
- Busca los primeros 5 contactos que tengan como género (gender) hombre (male): 
{"gender": "male"} y  el botón Options y establece el valor de Limit en 5.  || db.Contacts.find({"gender": "male"}).limit(5)

- Devuelve los 4 primeros contactos ordenados por nombre (name) de manera descendente:  
el valor de Sort en {"name.first": -1} y el valor de Limit en 4 || db.Contacts.find().sort({"name.first": -1}).limit(4)

- Clona la colección de Contacts a CopiaContacts y luego bórrala: 
 || db.Contacts.aggregate([{$match: {}}, {$out: "CopiaContacts"}]) y borrar: db.CopiaContacts.drop()
clic en el botón Collection y seleccionar Export Collection.
Seleccionar el formato de archivo y un nombre y ubicación para el archivo exportado.
clic en el botón Export para exportar los datos de la colección Contacts.
clic en el botón Database y selecciona Import Data.
Seleccionar el archivo exportado y elegir la base de datos y la colección de destino (por ejemplo, CopiaContacts).
clic en el botón Import para importar los datos en la nueva colección.

- Renombra el campo de name por nombre:  || db.Contacts.updateMany({}, {$rename: {"name": "nombre"}})
Selecciona la colección `Contacts`.
clic en el botón `Aggregations`. clic en el botón `Create` para crear una nueva agregación.
En la primera seleccionar `Add Stage` y seleccionar `$match` en el menú.
En el campo `$match`, ingresar JSON vacío `{}` para seleccionar todos.
clic en `Add Stage` nuevamente y selecciona `$out` en el menú desplegable.
En el campo `$out`, ingresar el nombre de una nueva colección temporal.
Haz clic en `Run` para ejecutar la agregación y copiar todos los documentos en la nueva colección temporal.
Haz clic en `Databases` y seleccionar la base de datos que contiene la colección `Contacts`.
Haz clic `...` junto al nombre de la colección `Contacts` y seleccionar `Drop Collection` para eliminar la colección original.
Haz clic en `...` junto al nombre de la colección temporal y seleccionar `Rename Collection`.
Ingresar el nombre original de la colección (`Contacts`) y clic en `Rename` para renombrar la colección temporal con el nombre original.

- Borra todos los contactos que tengan como estado (state) Florida: || db.contacts.deleteMany({"location.state": "Florida"})
Seleccionar la colección `contacts`.
En `Filter`, ingresar  {"location.state": "Florida"}
clic en el botón `Find`.
Seleccionar los documentos a eliminar haciendo clic en la casilla.
clic en el botón `Delete` para eliminar.


![ej-4](public/img/ej-4-.JPG)
![ej-4](public/img/ej-4a.JPG)
![ej-4](public/img/ej-4b.JPG)
![ej-4](public/img/ej-4c.JPG)
![ej-4](public/img/ej-4d.JPG)
![ej-4](public/img/ej-4e.JPG)
![ej-4](public/img/ej-4f.JPG)
![ej-4](public/img/ej-4g.JPG)
![ej-4](public/img/ej-4h.JPG)

------------------------------------------------------
------------------------------------------------------
# Ejercicio-3 
Crear una colección llamada Katas (retos de programación)

Cada documento deberá tener:

Name
Description
Level (nivel de dificultad numérico)
User (id asociado al usuario que lo ha creado)
Date (Fecha de creación del reto)
Valoration (sobre 5, valor numérico)
Chances (número de intentos realizados por otros usuarios)

Crear archivo de Kata.Entity.ts con el esquema Mongoose correspondiente

![ej-3](public/img/ej-3.JPG)

------------------------------------------------------
# Ejercicio-2
- Crear una ruta nueva:

api/goodbye

Debe devolver un JSON con una despedida:
{
“message”: “Goodbye {NOMBRE POR QUERY PARAMS}”,
“Date”: {FECHA ACTUAL}
}

Recuerda crear un type específico para esta respuesta dentro de la carpeta de controllers
Recuerda crear Controller y Router oportuno
Realiza pruebas con Postman y demuestra que funciona correctamente


  ![ej-2](public/img/ej-2.JPG)


